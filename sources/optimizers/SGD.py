from __future__ import absolute_import

from optimizers.BaseOptimizer import BaseOptimizer


class SGD(BaseOptimizer):
    def _do_step(self, layer):
        layer.weights = layer.weights - self._learning_rate * layer.dW

    def _compile(self, layer):
        pass
