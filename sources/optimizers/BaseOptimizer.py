

class BaseOptimizer:

    def __init__(self, learning_rate=1e-3):
        self._learning_rate = learning_rate

    def _do_step(self, layer):
        """
        :param layer: layer to optimize
        :return:
        """
        raise NotImplementedError('Implement layer optimization step')

    def do_optimize_step(self):
        """
        Making optimization step of compiled layers
        """
        for layer in self._trainable_layers:
            self._do_step(layer)

    def compile(self, layers):
        """
        Compiling layers. Do initialization of moments, history, e.t.c
        :param layers:
        :return:
        """
        self._trainable_layers = [layer for layer in layers if layer.trainable]
        for layer in self._trainable_layers:
            self._compile(layer)

    def _compile(self, layer):
        """
        Compiling one layer
        :param layer: BaseLayer instance
        :return:
        """
        raise NotImplementedError('Implement layer compilation')