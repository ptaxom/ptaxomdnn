from __future__ import absolute_import
from enum import Enum

class LearningPhase(Enum):
    """
    Model learning phase class.
    """
    TRAIN = 0
    INFERENCE = 1

from models.Model import Model