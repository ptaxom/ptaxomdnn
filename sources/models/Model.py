def _wrap(x):
    return x if type(x) in [tuple, list] else tuple([x])

def _unwrap(x):
    return x[0] if len(x) == 1 else x

class Model:

    def __init__(self, inputs, outputs, losses, optimizer):
        self.inputs = _wrap(inputs)
        self.outputs = _wrap(outputs)
        self.losses = _wrap(losses)
        self._init_graph()
        self._optimizer = optimizer
        self._optimizer.compile(self._layers)

    def _predict(self, X):
        X = _wrap(X)
        if len(X) != len(self.inputs):
            raise ValueError('Count of input data must match with count of inputs')
        for i in range(len(X)):
            self.inputs[i].forward(X[i])
        results = [output.result for output in self.outputs]
        return results[0] if len(results) == 0 else results

    def predict(self, X):
        return _unwrap(self._predict(X))

    def train(self, X, y):
        y = _wrap(y)
        if len(y) != len(self.outputs):
            raise ValueError('Count of input data must match with count of inputs')
        y_predicted = self._predict(X)
        losses = []
        for i in range(len(y)):
            output = self.outputs[i]._get_ref()
            total_loss = self.losses[i](y_predicted[i], y[i])
            losses.append(total_loss)
            print("Output {l_name}. Loss: {loss}".format(l_name=output.name, loss=total_loss))
            derivative = self.losses[i].derivative()
            output.backward(derivative)
        self._optimizer.do_optimize_step()
        return _unwrap(losses)

    def _init_graph(self):
        from queue import Queue
        nodes = set()
        to_visit = Queue()
        for inp in self.inputs:
            to_visit.put(inp)
            nodes.add(inp)
        while not to_visit.empty():
            node = to_visit.get()
            for sub_node in node._next_layers:
                if sub_node not in nodes:
                    nodes.add(sub_node)
                    to_visit.put(sub_node)
        self._layers = list(nodes)


