from __future__ import absolute_import
from layers.BaseLayer import BaseLayer
from layers.Activations import _layers_factory
import numpy as np


class Dense(BaseLayer):
    def __init__(self, units, activation=None, **kwargs):
        super().__init__((None, -1), name=kwargs.get('name', self._generate_layer_name()), **kwargs)
        self._output_shape = (None, units)
        # trick for built in activations in Dense layer
        self._ref = self
        self._activation = _layers_factory(activation)(self)
        self._ref = self._activation._get_ref()

    def __call__(self, prev_layer: BaseLayer, *args, **kwargs):
        input_shape = prev_layer.output_shape
        if len(input_shape) != 2 or not input_shape[0] is None or input_shape[1] <= 0:
            raise ValueError('Incompatible shapes of layers. Previous layer mast be of shape (None, x_units). Use '
                             'Flatten layer before this.')
        self._input_shape = input_shape
        h = input_shape[1] + 1
        w = self._output_shape[1]
        self._weights = np.random.randn(h, w) / np.sqrt((h + w))
        self._add_backward_layer(prev_layer)
        return self

    def _forward(self, X):
        biases = np.ones((X.shape[0], 1))
        X = np.hstack([biases, X])
        self._X = X
        y = X @ self._weights
        return y

    def _backward(self, error):
        back_error = error @ self._weights[1:, :].T
        batch_size = self._X.shape[0]
        self._dw = self._X.T @ error / batch_size
        return back_error

    def _get_ref(self):
        return self._ref

    def get_layers(self):
        return [self, self._activation]

    @property
    def weights(self):
        return self._weights

    @weights.setter
    def weights(self, new_weights):
        self._weights = new_weights

    @property
    def dW(self):
        return self._dw
