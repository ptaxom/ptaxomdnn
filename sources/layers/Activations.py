from __future__ import absolute_import
from layers.BaseLayer import BaseLayer
import numpy as np
import sys
import inspect


class BaseActivation(BaseLayer):
    def __init__(self, **kwargs):
        super().__init__((None, -1),
                         name=kwargs.get('name', self._generate_layer_name()),
                         training=False,
                         **kwargs)

    def __call__(self, prev_layer: BaseLayer, *args, **kwargs):
        self._add_backward_layer(prev_layer)
        self._input_shape = prev_layer.output_shape
        self._output_shape = prev_layer.output_shape
        return self

    def _forward(self, X):
        return X


class Linear(BaseActivation):
    def _forward(self, X):
        return X

    def _backward(self, error):
        return error


class Sigmoid(BaseActivation):

    def _forward(self, X):
        self._y = 1. / (1. + np.exp(-X))
        return self._y

    def _backward(self, error):
        derivative = np.multiply(self._y, 1. - self._y)
        return np.multiply(derivative, error)


class TANH(BaseActivation):

    def _forward(self, X):
        self._y = np.tanh(X)
        return self._y


class ReLU(BaseActivation):

    def _forward(self, X):
        self._y = np.maximum(0, X)
        return self._y


class Softmax(BaseActivation):

    def _compute_jacobian(self):
        _y = self._y
        batch_size, dim = _y.shape
        jacobian = np.zeros((batch_size, dim, dim))
        for i in range(batch_size):
            jacobian[i] = np.diag(_y[i]) - np.outer(_y[i], _y[i])
        self._jacobian = np.mean(jacobian, axis=0)


    def _forward(self, X):
        _y = np.exp(X - X.max())
        sums = np.sum(_y, axis=1)
        t_sums = np.tile(sums, (self._output_shape[-1], 1)).T
        self._y = _y / t_sums
        return self._y

    def _backward(self, error):
        self._compute_jacobian()
        return error @ self._jacobian

def _layers_factory(obj):
    if obj is None:
        return Linear()
    if issubclass(type(obj), BaseActivation):
        return obj
    layer_name = obj
    classes_dict = dict([(class_name.lower(), class_obj) for class_name, class_obj in
                         inspect.getmembers(sys.modules[__name__], inspect.isclass)])
    layer_name = 'linear' if layer_name is None else layer_name
    return classes_dict.get(layer_name)()
