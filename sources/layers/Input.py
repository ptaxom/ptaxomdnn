from __future__ import absolute_import
from layers.BaseLayer import BaseLayer


class Input(BaseLayer):
    def __init__(self, input_shape, **kwargs):
        super().__init__(input_shape,
                         name=kwargs.get('name', self._generate_layer_name()),
                         training=False,
                         **kwargs)
        self._output_shape = self._input_shape

    def _forward(self, X):
        return X

    def _backward(self, errors):
        pass