from __future__ import absolute_import
from models import LearningPhase
import numpy as np

class MetaLayer(type):
    """
    Metaclass for generating initial variables like name formatter for layers
    """
    def __new__(cls, clsname, superclasses, attributedict):
        attributedict['_layer_name_pattern'] = clsname + "_{layer_num}"
        attributedict['_layer_nums'] = 0
        return type.__new__(cls, clsname, superclasses, attributedict)


class BaseLayer(metaclass=MetaLayer):
    """
    Base class for all layers
    """
    # Example of initializing names
    _layer_name_pattern = "BaseLayer_{layer_num}"
    _layer_nums = 0

    def __init__(self, input_shape, **kwargs):
        self._name = kwargs.get('name', 'BaseLayer')
        self._output_shape = kwargs.get('output_shape', None)
        self._is_training = kwargs.get('training', True)
        self._learning_phase = kwargs.get('learning_phase', LearningPhase.TRAIN)
        if input_shape[0] is None:
            self._input_shape = input_shape
        else:
            self._input_shape = tuple([None] + [dim for dim in input_shape])
        self._next_layers = []
        self._backward_layers = []
        self._result = None

    @property
    def name(self):
        """
        :return: name of the layer
        """
        return self._name

    @property
    def input_shape(self):
        """
        :return: shape of the input tensor
        """
        return self._input_shape

    @property
    def output_shape(self):
        """
        :return: shape of the output tensor
        """
        return self._output_shape

    def _forward(self, X):
        """
        Make computations for forward pass of the layer
        :param X: input tensor with shape (batch_size, input_shape)
        :return: output tensor with shape (batch_size, output_shape)
        """
        raise NotImplemented

    def forward(self, X):
        """
        Make forward pass of the layer
        :param X: input tensor with shape (batch_size, input_shape)
        :return: output tensor with shape (batch_size, output_shape)
        """
        x = np.copy(X)
        self._result = self._forward(x)
        for layer in self._next_layers:
            layer.forward(self._result)

    def _backward(self, error):
        """
        Make backward pass of the layer and train network
        :param errors: input error of the layer
        :return: output error of the layer
        """
        raise NotImplementedError

    def backward(self, error):
        def _wrap(x):
            return x if type(x) in [tuple, list] else tuple([x])

        error = np.copy(error)
        self._errors = _wrap(self._backward(error))
        for i in range(len(self._backward_layers)):
            self._backward_layers[i].backward(self._errors[i])

    def _generate_layer_name(self):
        pattern = self.__class__._layer_name_pattern
        layer_num = self.__class__._layer_nums
        self.__class__._layer_nums += 1
        return pattern.format(layer_num=layer_num)

    def _add_backward_layer(self, layer):
        """
        Add previous layer for this layer and save this layer as next of previous
        :param layer:
        :return:
        """
        self._backward_layers.append(layer._get_ref())
        layer._next_layers.append(self)

    def _get_ref(self):
        """
        Return reference of layer, which will be used in _add_backward_layer. Usefull for built-in layers, like activation in Dense/Conv
        :return: reference
        """
        return self

    @property
    def trainable(self):
        return self._is_training

    @trainable.setter
    def trainable(self, b: bool):
        self._is_training = b

    @property
    def result(self):
        return self._get_ref()._result

    def get_layers(self):
        return [self]

    @property
    def weights(self):
        raise NotImplementedError

    @weights.setter
    def weights(self, new_weights):
        raise NotImplementedError

    @property
    def dW(self):
        raise NotImplementedError