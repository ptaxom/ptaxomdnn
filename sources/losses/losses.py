import numpy as np


class BaseLoss:

    def __call__(self, prediction, target):
        raise NotImplementedError

    def derivative(self):
        raise NotImplementedError

class MSE(BaseLoss):

    def __call__(self, prediction, target):
        total_loss = np.mean((target - prediction) ** 2) / 2
        self._derivatives = prediction - target
        return total_loss

    def derivative(self):
        return self._derivatives


class LogLoss(BaseLoss):
    def __call__(self, prediction, target):
        prediction = np.clip(prediction, 0.01, 1)
        loss = np.mean(-np.sum(np.multiply(target, np.log(prediction)), axis=1))
        self._derivatives = -target / prediction
        return np.mean(loss)

    def derivative(self):
        return self._derivatives
