from layers import Input, Dense, Linear
from models import Model
import numpy as np
from losses import MSE, LogLoss
from optimizers import SGD
import matplotlib.pyplot as plt

def create_model(inputs, outputs):
    inp = Input(input_shape=(None, inputs))
    X = inp
    X = Dense(20, 'sigmoid')(X)
    X = Dense(outputs, 'softmax')(X)
    model = Model(inputs=inp, outputs=X, losses=LogLoss(), optimizer=SGD(learning_rate=1e-3))
    return model

def generate_one_hot(batch_size, outputs):
    y = np.zeros((batch_size, outputs))
    targets = np.random.randint(0, outputs, batch_size)
    y[np.arange(targets.size), targets] = 1
    return y

def accuracy(y_hat, y):
    y_hat = np.argmax(y_hat, axis=1)
    y = np.argmax(y, axis=1)
    return np.sum((y_hat == y) * 1) / y.size

if __name__ == '__main__':
    # np.random.seed(42)
    inputs, outputs = 30, 3
    model = create_model(inputs, outputs)
    train_samples = 1024
    x = np.random.randn(train_samples, inputs)
    target = generate_one_hot(train_samples, outputs)
    acc_before = accuracy(model.predict(x), target)
    losses = []
    epochs = 20
    for i in range(epochs):
        print("Epoch: ", i)
        loss = model.train(x, target)
        losses.append(loss)
    acc_after = accuracy(model.predict(x), target)
    print(acc_before, acc_after)
    plt.title('Loss')
    plt.plot(range(epochs), losses)
    plt.show()